import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { PetModule } from '../src/modules/pet/pet.module';
import { PetService } from '../src/modules/pet/pet.service';

describe('PetController (e2e)', () => {
  let app: INestApplication;

  const pet1 = {
    _key: '1',
    name: 'Remy',
    breed: 'Parsi',
    type: 'Cat',
    personKey: '1',
  };

  const pet2 = {
    _key: '1',
    name: 'Doge',
    breed: 'Parsi',
    type: 'Cat',
    personKey: '1',
  };
  let petsArray = [pet1];
  const petService = {
    getAll: () => petsArray,
    insertOne: (data) => {
      data._key = pet2._key;
      petsArray.push(data);
      return { _key: data._key };
    },
    getByKey: (_key: string) => {
      return petsArray.find((x) => x._key === _key);
    },
    remove: (_key: string) => {
      petsArray = petsArray.filter((x) => x._key !== _key);
      return true;
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [PetModule],
    })
      .overrideProvider(PetService)
      .useValue(petService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/pets (POST)', () => {
    return request(app.getHttpServer())
      .post('/pets')
      .send({
        name: pet2.name,
        breed: pet2.breed,
        type: pet2.type,
        personKey: pet2.personKey,
      })
      .expect(201)
      .expect({ _key: pet2._key });
  });

  it('/pets (GET)', () => {
    return request(app.getHttpServer())
      .get('/pets')
      .expect(200)
      .expect(petService.getAll());
  });

  it('/pets/{id} (GET)', () => {
    return request(app.getHttpServer())
      .get('/pets/1')
      .expect(200)
      .expect(petService.getByKey('1'));
  });

  it('/pets/{id} (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/pets/1')
      .expect(200)
      .expect('true');
  });
});
