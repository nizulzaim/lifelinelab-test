import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { DatabaseModule } from '../src/modules/database/database.module';
import { PersonModule } from '../src/modules/person/person.module';
import { PetModule } from '../src/modules/pet/pet.module';
import { PersonService } from '../src/modules/person/person.service';

describe('PersonController (e2e)', () => {
  let app: INestApplication;
  const person1 = {
    _key: '1',
    name: 'Nizul Zaim',
    email: 'mnizulzaim@gmail.com',
  };
  const person2 = {
    _key: '2',
    name: 'Lili Madiha',
    email: 'lilimadiha@gmail.com',
  };
  const pet1 = {
    _key: '1',
    name: 'Remy',
    breed: 'Parsi',
    type: 'Cat',
    personKey: '1',
  };
  let personArray = [person1];
  const petsArray = [pet1];
  const personService = {
    getAll: () => personArray,
    insertOne: (data) => {
      data._key = person2._key;
      personArray.push(data);
      return { _key: data._key };
    },
    getByKey: (_key: string) => {
      return personArray.find((x) => x._key === _key);
    },
    getPetsByPerson: (_key: string) => {
      return petsArray.filter((x) => x.personKey === _key);
    },
    removePerson: (_key: string) => {
      personArray = personArray.filter((x) => x._key !== _key);
      return true;
    },
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule, PersonModule, PetModule],
    })
      .overrideProvider(PersonService)
      .useValue(personService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/persons (POST)', () => {
    return request(app.getHttpServer())
      .post('/persons')
      .send({ name: person2.email, email: person2.email })
      .expect(201)
      .expect({ _key: person2._key });
  });

  it('/persons (GET)', () => {
    return request(app.getHttpServer())
      .get('/persons')
      .expect(200)
      .expect(personService.getAll());
  });

  it('/persons/{id} (GET)', () => {
    return request(app.getHttpServer())
      .get('/persons/1')
      .expect(200)
      .expect(personService.getByKey('1'));
  });

  it('/persons/{id} (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/persons/1')
      .expect(200)
      .expect('true');
  });

  it('/persons/{id}/pets (GET)', () => {
    return request(app.getHttpServer())
      .get('/persons/1/pets')
      .expect(200)
      .expect(personService.getPetsByPerson('1'));
  });
});
