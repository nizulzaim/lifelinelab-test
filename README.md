## Description

Using [Nest](https://github.com/nestjs/nest) framework for Person and Pet API endpoint. This package include following stack:

1. NestJS with Express
2. Typescript
3. Unit testing using mocha, chai and sinon
4. E2E testing using jest
5. Swagger for API documentation
6. ArangoDB for database layer

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

```

## Database Diagram
Here is the following ERD Diagram for the project

![erd diagram](https://gitlab.com/nizulzaim/lifelinelab-test/-/raw/master/docs/erd.jpeg)

## Configuration file
The following configuration required in root folder with the filename `.env`:

```
DB_HOST='http://127.0.0.1:8529'
DB_USERNAME='root'
DB_PASSWORD=''
DB_NAME='lifelinelabs_test'
```

## What's next?

1. Better Joi validation
2. Code coverage report using mocha
3. Custom database connector which support widerange arangodb feature


