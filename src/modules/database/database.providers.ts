// import { createConnection } from 'typeorm';
import { Database } from 'arangojs';
import { DATABASE_CONNECTION_TOKEN } from '../../app.constants';

// Create Provider for Database Connection
// Others Module importing this provider will wait for connection to establish first
export const databaseProviders = [
  {
    provide: DATABASE_CONNECTION_TOKEN,
    useFactory: async (): Promise<Database> => {
      const db = new Database(process.env.DB_HOST);
      db.useBasicAuth(process.env.DB_USERNAME, process.env.DB_PASSWORD);
      db.database(process.env.DB_NAME);
      return db;
    },
  },
];
