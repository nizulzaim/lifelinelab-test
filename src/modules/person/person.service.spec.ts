import { Test } from '@nestjs/testing';
import { expect } from 'chai';
import { DatabaseModule } from '../database/database.module';
import { petsProviders } from '../pet/pet.providers';
import { PersonController } from './person.controller';
import { personProviders } from './person.providers';
import { PersonService } from './person.service';
import { SinonStub } from 'sinon';
import * as sinon from 'sinon';
import { Person } from './person.entity';
import { DocumentCollection } from 'arangojs/collection';
import { plainToClass } from 'class-transformer';
import { ConflictException } from '@nestjs/common';

describe('PersonService', () => {
  let sandbox: sinon.SinonSandbox;
  let personService: PersonService;
  let personCollection: DocumentCollection<Person>;

  beforeEach(async () => {
    sandbox = sinon.createSandbox();
    const moduleRef = await Test.createTestingModule({
      providers: [PersonService, ...personProviders, ...petsProviders],
      controllers: [PersonController],
      imports: [DatabaseModule],
    }).compile();
    personService = moduleRef.get<PersonService>(PersonService);
    personCollection = await moduleRef.resolve('PERSON_COLLECTION');
  });

  describe('#getAll()', () => {
    let allStub: SinonStub;

    beforeEach(async () => {
      allStub = sandbox.stub(personCollection, 'all');
    });

    it('should call getAll', async () => {
      allStub.callsFake(() => Promise.resolve([]));
      await personService.getAll({});
      expect(allStub.calledOnce).to.be.true;
    });

    it('should call getAll and return 1 data', async () => {
      const result: Person = {
        _key: '10001',
        email: 'mnizulzaim@gmail.com',
        name: 'Nizul Zaim',
      };
      allStub.callsFake(() => Promise.resolve([result]));
      const res = await personService.getAll({});
      expect(res.length).to.be.equal(1);
    });

    it('should call getAll and return Person object', async () => {
      const result = {
        _key: '10001',
        email: 'mnizulzaim@gmail.com',
        name: 'Nizul Zaim',
      };
      allStub.callsFake(() => Promise.resolve([result]));
      const classResult = plainToClass(Person, result, {
        excludeExtraneousValues: true,
      });
      const res = await personService.getAll({});
      expect(res).to.be.eql([classResult]);
    });
  });

  describe('#getByKey()', () => {
    let lookupByKeys: SinonStub;
    const personObject = {
      _key: '10001',
      email: 'mnizulzaim@gmail.com',
      name: 'Nizul Zaim',
    };

    beforeEach(async () => {
      lookupByKeys = sandbox.stub(personCollection, 'lookupByKeys');
    });

    it('should call lookupByKeys', async () => {
      lookupByKeys.callsFake(() => Promise.resolve([]));
      await personService.getByKey('10001');
      expect(lookupByKeys.calledOnce).to.be.true;
    });

    it('should return Person object', async () => {
      lookupByKeys.callsFake(() => Promise.resolve([personObject]));
      const res = await personService.getByKey('10001');
      const person = plainToClass(Person, personObject, {
        excludeExtraneousValues: true,
      });
      expect(res).to.be.eql(person);
    });
  });

  describe('#insertOne()', () => {
    let saveStub: SinonStub;
    let checkPersonExistStub: SinonStub;

    beforeEach(async () => {
      saveStub = sandbox.stub(personCollection, 'save');
      checkPersonExistStub = sandbox.stub(
        personService,
        'checkPersonExistByEmail',
      );
      //   byExampleStub = sandbox.stub(personCollection, 'byExample');
    });

    it('should call checkPersonExistStub', async () => {
      checkPersonExistStub.callsFake(() => Promise.resolve(false));

      await personService.insertOne({
        email: 'lilimadiha@gmail.com',
        name: 'nizulzaim',
      });
      expect(checkPersonExistStub.calledOnce).to.be.true;
    });

    it('should return ConflictException', async () => {
      checkPersonExistStub.callsFake(() => Promise.resolve(true));

      try {
        await personService.insertOne({
          email: 'lilimadiha@gmail.com',
          name: 'Lili Madiha',
        });
      } catch (err) {
        expect(err).to.be.instanceOf(ConflictException);
      }
    });

    it('should return result', async () => {
      checkPersonExistStub.callsFake(() => Promise.resolve(false));
      saveStub.callsFake(() => {
        _key: 1;
      });

      try {
        const res = await personService.insertOne({
          email: 'lilimadiha@gmail.com',
          name: 'Lili Madiha',
        });
        expect(res).to.be.eql({ _key: 1 });
      } catch (err) {
        expect(err).to.be.not.instanceOf(ConflictException);
      }
    });
  });
});
