import {
  ConflictException,
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreatePersonDto } from './dto/create-person.dto';
import { plainToClass } from 'class-transformer';
import { Person } from './person.entity';
import { DocumentCollection } from 'arangojs/collection';
import { Pet } from '../pet/pet.entity';

@Injectable()
export class PersonService {
  constructor(
    @Inject('PERSON_COLLECTION')
    private personCollection: DocumentCollection<Person>,
    @Inject('PET_COLLECTION')
    private petCollection: DocumentCollection<Pet>,
  ) {}

  async insertOne(body: CreatePersonDto): Promise<any> {
    try {
      const isExist = await this.checkPersonExistByEmail(body.email);
      if (isExist) throw new ConflictException();

      const person = await this.personCollection.save(body, {
        returnNew: true,
      });
      return plainToClass(Person, person, { excludeExtraneousValues: true });
    } catch (err) {
      if (err instanceof ConflictException) throw err;
      throw new ForbiddenException();
    }
  }

  async checkPersonExistByEmail(email: string): Promise<boolean> {
    try {
      const query = await this.personCollection.byExample({ email });
      const isExist = (await query.all()).length > 0;

      return isExist;
    } catch (err) {
      throw new ForbiddenException();
    }
  }

  async addFriend(_key: string, friendKey: string): Promise<string[]> {
    const [person, friend] = await Promise.all([
      this.personCollection.lookupByKeys([_key]),
      this.personCollection.lookupByKeys([friendKey]),
    ]);

    if (!person || !friend) throw new NotFoundException();
    const personKeys = person[0].friendKeys ? person[0].friendKeys : [];
    const friendKeys = friend[0].friendKeys ? friend[0].friendKeys : [];
    personKeys.push(friendKey);
    friendKeys.push(_key);

    await Promise.all([
      this.personCollection.update(person[0]._key, {
        friendKeys: personKeys,
      }),
      this.personCollection.update(friend[0]._key, {
        friendKeys,
      }),
    ]);

    return personKeys;
  }

  async getFriend(_key: string): Promise<Person[]> {
    try {
      const person = await this.personCollection.lookupByKeys([_key]);

      if (!person) throw new NotFoundException();
      const friends = await this.personCollection.lookupByKeys(
        person[0].friendKeys,
      );
      return friends.map((item: Person) => {
        return plainToClass(Person, item, {
          excludeExtraneousValues: true,
        });
      }) as Person[];
    } catch (err) {
      if (err instanceof NotFoundException) throw err;
      throw new ForbiddenException();
    }
  }

  async getByKey(_key: string): Promise<Person> {
    try {
      const result = await this.personCollection.lookupByKeys([_key]);
      const res = plainToClass(Person, result[0], {
        excludeExtraneousValues: true,
      });
      return res;
    } catch (err) {
      throw new ForbiddenException();
    }
  }

  async getAll(opts: any): Promise<Person[]> {
    try {
      const results = await this.personCollection.all(opts);
      return results.map((item: Person) => {
        return plainToClass(Person, item, {
          excludeExtraneousValues: true,
        });
      }) as Promise<Person[]>;
    } catch (err) {
      throw new ForbiddenException();
    }
  }

  async getPetsByPerson(_key: string): Promise<Pet[]> {
    try {
      const result = await this.petCollection.byExample({ personKey: _key });
      return result.map((item) => {
        return plainToClass(Pet, item, {
          excludeExtraneousValues: true,
        });
      });
    } catch (err) {
      throw new ForbiddenException();
    }
  }

  async removePerson(_key: string): Promise<boolean> {
    try {
      await this.personCollection.remove({ _key: _key });
      return true;
    } catch (err) {
      if (err.message === 'document not found') {
        throw new NotFoundException();
      }
      throw new ForbiddenException();
    }
  }
}
