import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { petsProviders } from '../pet/pet.providers';
import { PersonController } from './person.controller';
import { personProviders } from './person.providers';
import { PersonService } from './person.service';
import { JoiPipeModule } from 'nestjs-joi';

@Module({
  imports: [DatabaseModule, JoiPipeModule],
  controllers: [PersonController],
  providers: [PersonService, ...personProviders, ...petsProviders],
})
export class PersonModule {}
