import { DATABASE_CONNECTION_TOKEN } from '../../app.constants';
import { Database } from 'arangojs';
export const personProviders = [
  {
    provide: 'PERSON_COLLECTION',
    useFactory: async (db: Database) => {
      const collection = db.collection('persons');
      if (!(await collection.exists())) {
        await collection.create();
      }
      return collection;
    },
    inject: [DATABASE_CONNECTION_TOKEN],
  },
];
