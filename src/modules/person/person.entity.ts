import { Expose } from 'class-transformer';

export class Person {
  @Expose()
  _key?: string;
  @Expose()
  email: string;
  @Expose()
  name: string;

  @Expose()
  friendKeys?: string[];
}
