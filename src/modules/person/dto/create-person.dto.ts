import { ApiProperty } from '@nestjs/swagger';
import { JoiSchemaOptions, JoiSchema } from 'nestjs-joi';
import * as Joi from 'joi';
@JoiSchemaOptions({
  allowUnknown: false,
})
export class CreatePersonDto {
  @JoiSchema(Joi.string().required().email())
  @ApiProperty()
  email: string;
  @ApiProperty()
  @JoiSchema(Joi.string().required().min(5).max(50))
  name: string;
}
