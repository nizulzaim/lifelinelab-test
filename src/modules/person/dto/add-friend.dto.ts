import { ApiProperty } from '@nestjs/swagger';
import { JoiSchemaOptions, JoiSchema } from 'nestjs-joi';
import * as Joi from 'joi';
@JoiSchemaOptions({
  allowUnknown: false,
})
export class AddFriendDto {
  @JoiSchema(Joi.string().required())
  @ApiProperty()
  _key: string;
}
