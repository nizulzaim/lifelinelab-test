import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CreatePersonDto } from './dto/create-person.dto';
import { Person } from './person.entity';
import { PersonService } from './person.service';
import { ApiTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { AddFriendDto } from './dto/add-friend.dto';

@ApiTags('Person')
@Controller('persons')
export class PersonController {
  constructor(private readonly personService: PersonService) {}
  @ApiOperation({ summary: 'Get All Person' })
  @ApiResponse({ status: 200, description: 'Return all Perons' })
  @Get()
  async getAll(): Promise<any> {
    return this.personService.getAll({});
  }

  @ApiOperation({ summary: 'Get one Person based on id' })
  @ApiResponse({ status: 200, description: 'Return Person object' })
  @Get(':id')
  async getByKey(@Param('id') id: string): Promise<Person> {
    return this.personService.getByKey(id);
  }

  @ApiOperation({ summary: 'Create Person' })
  @ApiResponse({ status: 200, description: 'Return _key which containing id' })
  @Post()
  async addOne(@Body() theOne: CreatePersonDto) {
    return this.personService.insertOne(theOne);
  }

  @ApiOperation({ summary: 'Get Pets by Person' })
  @ApiResponse({ status: 200, description: 'Return pet array' })
  @Get(':id/pets')
  async getPetsByPerson(@Param('id') id: string) {
    return this.personService.getPetsByPerson(id);
  }

  @ApiOperation({ summary: 'Delete Person' })
  @ApiResponse({ status: 200, description: 'Return _key which containing id' })
  @Delete(':id')
  async removeOne(@Param('id') id: string): Promise<boolean> {
    return this.personService.removePerson(id);
  }

  @ApiOperation({ summary: 'Add Friend to person' })
  @ApiResponse({ status: 200, description: 'Return all current friends' })
  @Patch(':id/friends')
  async addFriend(@Param('id') id: string, @Body() addFriendDto: AddFriendDto) {
    return this.personService.addFriend(id, addFriendDto._key);
  }

  @ApiOperation({ summary: 'Get Friends by Person' })
  @ApiResponse({ status: 200, description: 'Return friends array' })
  @Get(':id/friends')
  async getFriendsByPerson(@Param('id') id: string) {
    return this.personService.getFriend(id);
  }
}
