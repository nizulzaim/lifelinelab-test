import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { PetController } from './pet.controller';
import { petsProviders } from './pet.providers';
import { PetService } from './pet.service';
import { JoiPipeModule } from 'nestjs-joi';

@Module({
  imports: [DatabaseModule, JoiPipeModule],
  controllers: [PetController],
  providers: [PetService, ...petsProviders],
})
export class PetModule {}
