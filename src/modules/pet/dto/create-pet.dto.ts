import { ApiProperty } from '@nestjs/swagger';
import { PetType } from '../pet.entity';
import { JoiSchemaOptions, JoiSchema } from 'nestjs-joi';
import * as Joi from 'joi';
@JoiSchemaOptions({
  allowUnknown: false,
})
export class CreatePetDto {
  @JoiSchema(Joi.string().required())
  @ApiProperty()
  name: string;

  @JoiSchema(Joi.string().required().allow('Dog').allow('Cat').allow('Hamster'))
  @ApiProperty({ enum: PetType })
  type: PetType;

  @JoiSchema(Joi.string().optional())
  @ApiProperty()
  breed?: string;

  @JoiSchema(Joi.string().required())
  @ApiProperty()
  personKey: string;
}
