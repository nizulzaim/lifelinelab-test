import {
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
// import {DatabaseModule} from '../database/database.module';
// import { ArangoBaseService } from '../database/ArangoBaseService';
import { CreatePetDto } from './dto/create-pet.dto';
import { plainToClass } from 'class-transformer';

import { DocumentCollection } from 'arangojs/collection';
import { Pet } from './pet.entity';

@Injectable()
export class PetService {
  constructor(
    @Inject('PET_COLLECTION')
    private collection: DocumentCollection<Pet>,
  ) {}

  async insertOne(body: CreatePetDto): Promise<any> {
    const pet = await this.collection.save(body, { returnNew: true });
    return plainToClass(Pet, pet, { excludeExtraneousValues: true });
  }

  async getByKey(_key: string): Promise<Pet> {
    const result = await this.collection.lookupByKeys([_key]);
    const res = plainToClass(Pet, result[0], {
      excludeExtraneousValues: true,
    });
    return res;
  }

  async getAll(opts: any): Promise<any[]> {
    const results = await this.collection.all(opts);
    return results.map((item) => {
      return plainToClass(Pet, item, {
        excludeExtraneousValues: true,
      });
    });
  }

  async remove(_key: string): Promise<boolean> {
    try {
      await this.collection.remove({ _key: _key });
      return true;
    } catch (err) {
      if (err.message === 'document not found') {
        throw new NotFoundException();
      }
      throw new ForbiddenException();
    }
  }
}
