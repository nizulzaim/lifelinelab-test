import { DATABASE_CONNECTION_TOKEN } from '../../app.constants';
import { Database } from 'arangojs';
export const petsProviders = [
  {
    provide: 'PET_COLLECTION',
    useFactory: async (db: Database) => {
      const collection = db.collection('pets');
      if (!(await collection.exists())) {
        await collection.create();
      }
      return collection;
    },
    inject: [DATABASE_CONNECTION_TOKEN],
  },
];
