import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { CreatePetDto } from './dto/create-pet.dto';
import { Pet } from './pet.entity';
import { PetService } from './pet.service';
@ApiTags('Pet')
@Controller('pets')
export class PetController {
  constructor(private readonly petService: PetService) {}
  @ApiOperation({ summary: 'Get All Pets' })
  @ApiResponse({ status: 200, description: 'Return all Pets' })
  @Get()
  async getAll(): Promise<any> {
    return this.petService.getAll({});
  }

  @ApiOperation({ summary: 'Get one Pet based on id' })
  @ApiResponse({ status: 200, description: 'Return Person object' })
  @Get(':id')
  async getByKey(@Param('id') id: string): Promise<Pet> {
    return this.petService.getByKey(id);
  }

  @ApiOperation({ summary: 'Create Pet' })
  @ApiResponse({ status: 200, description: 'Return _key which containing id' })
  @Post()
  async addOne(@Body() theOne: CreatePetDto) {
    return this.petService.insertOne(theOne);
  }

  @ApiOperation({ summary: 'Delete Pet' })
  @ApiResponse({ status: 200, description: 'Return "true"' })
  @Delete(':id')
  async removeOne(@Param('id') id: string): Promise<boolean> {
    return this.petService.remove(id);
  }
}
