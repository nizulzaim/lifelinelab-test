import { Expose } from 'class-transformer';

export enum PetType {
  Dog = 'Dog',
  Cat = 'Cat',
  Hamster = 'Hamster',
}

export class Pet {
  @Expose()
  _key?: string;

  @Expose()
  name: string;

  @Expose()
  type: PetType;

  @Expose()
  breed?: string;

  @Expose()
  personKey: string;
}
