import { Module } from '@nestjs/common';
import { DatabaseModule } from './modules/database/database.module';
import { PersonModule } from './modules/person/person.module';
import { PetModule } from './modules/pet/pet.module';

@Module({
  imports: [DatabaseModule, PersonModule, PetModule],
})
export class AppModule {}
